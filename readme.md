
- install pycharm
- intall python 3
- install anaconda 
- create and use new virtual environment from terminal
https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/20/conda/

- install packages by going to requirements.txt
pip install -r requirements.txt

- install postresql and pgadmin
- create a new database in pgadmin named jobsdb
https://www.youtube.com/watch?v=d--mEqEUybA
https://www.youtube.com/watch?v=69YkZqZgz9s

-django commands:
  - python manage.py makemigrations
  - python manage.py migrate
  - python manage.py runserver
  - python manage.py createsuperuser

-admin backend url:
 - 127.0.0.1:8000/admin

- upload csv named combined_jobs_final.csv
- all the recommendations are preprocessed and saved to database from folder jobs/finalone
 - clean_data.py -> get the data from csv and preprocess the data to remove stop words, html ,etc. and save to test.csv
 
Steps:
- for database import
 - run the site and import the filtered_software_developer_data.csv from folder finalone
 - run py file: import_Database.py

- if you want recommendation for specific job go to that job detail from site and select:
 any recommendation by tfidf/word2vec/wmd
 - if the data is in the database it loads faster but if not then it is calculated which might take about 2-3 minutes
 
- for providing recommendation to whole job dataset go to db_save_*.py for each tf-idf, word2vec or wmd in folder finalone
  -for how many job to generate recommendation 
   - db_save_tfidf.py -> in line 63 change -> all_jobs = Job.objects.all()[:1000] 
   - db_save_tfidf.py -> in line 76 change -> all_jobs = Job.objects.all()[:1000] 
   - db_save_tfidf.py -> in line 65 change -> all_jobs = Job.objects.all()[:1000] 
    
    NOte: this goes from 1 to 1000 jobs from start
    if you want to resume from lets say 200 jobs upto 500 jobs then do Job.objects.all()[200:500]


Other preprocessing files:
 -clean_data.py -> clean the data and save it for further use
 - trainword2vec.py -> train the word2vec model and save to word2vecmodel.txt
 - db_save_tfidf.py
    -> uses test.csv 
    -> stores top 10 recommendations from tf-idf to db of each jobs(training takes a lot of time)
 - db_Save_word2vec.py
 (word2vec model needs huge data so we take data from two datasets)
    -> uses test.csv
    -> pretrained model - word2vecmodel.txt 
    -> uses spacy library to get top 10 recommendations from word2vec of each jobs in db
 - db_Save_word2vec_wmd.py -> uses wmd library to get top 10 recommendations from word2vec

