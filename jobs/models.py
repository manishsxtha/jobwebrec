from django.contrib.auth.models import User
from django.db import models


class Job(models.Model):
    id = models.IntegerField(primary_key=True)
    job_title = models.TextField(default='')
    category = models.TextField(default='')
    job_description = models.TextField(default='')
    city = models.TextField(default='')
    company = models.TextField(default='')

    def __str__(self):
        return self.job_title


class Recommendtfidf(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    rec_job_id = models.TextField(default='')
    rec_job_title = models.TextField(default='')
    rec_job_score = models.TextField(default='')

    def __str__(self):
        return self.rec_job_title


class Recommendw2v(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    rec_job_id = models.TextField(default='')
    rec_job_title = models.TextField(default='')
    rec_job_score = models.TextField(default='')

    def __str__(self):
        return self.rec_job_title


class RecommendWMD(models.Model):
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    rec_job_id = models.TextField(default='')
    rec_job_title = models.TextField(default='')
    rec_job_score = models.TextField(default='')

    def __str__(self):
        return self.rec_job_title


class RateRecommendtfidf(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    recommendtfidf = models.ForeignKey(Recommendtfidf, on_delete=models.CASCADE)
    rating = models.IntegerField(default=1)

    def __str__(self):
        return self.recommendtfidf.job.job_title


class RateRecommendw2v(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    recommendw2v = models.ForeignKey(Recommendw2v, on_delete=models.CASCADE)
    rating = models.IntegerField(default=1)

    def __str__(self):
        return self.recommendw2v.job.job_title


class RateRecommendWMD(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    job = models.ForeignKey(Job, on_delete=models.CASCADE)
    recommendWMD = models.ForeignKey(RecommendWMD, on_delete=models.CASCADE)
    rating = models.IntegerField(default=1)

    def __str__(self):
        return self.recommendWMD.job.job_title
