from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

from . import views

urlpatterns = [
                  path('', views.index, name='index'),
                  path('job-detail/<int:job_id>/', views.job_detail, name='job-detail'),
                  path('job-search/', views.job_list, name='job-search'),
                  path('job-upload/', views.import_jobs, name='job-upload'),
                  path('recommend-tfidf/<int:job_id>/', views.recommendations_tfidf, name='recommendation-tfidf'),
                  path('recommend-word2vec/<int:job_id>/', views.recommendations_word2vec,
                       name='recommendation-word2vec'),
                  path('recommend-word2vec-wmd/<int:job_id>/', views.recommendations_word2vec_wmd,
                       name='recommendation-word2vec-wmd'),
                  path('tfidf-rate/', views.tfidf_rating, name='rate-recommendation-tfidf'),
                  path('w2v-rate/', views.word2vec_rating, name='rate-recommendation-w2v'),
                  path('wmd-rate/', views.word2vec_wmd_rating, name='rate-recommendation-wmd'),

              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
