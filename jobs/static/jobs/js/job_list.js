$(function () {

    submit_rating_data()

});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function submit_rating_data() {
    $('#submit-rating').click(function (e) {
            e.preventDefault();


            //TODO: convert form to json format for android and web compatibility
            form = $("#recommendation-rating-form");

            parent = $(this).parent()
            inputs = $(this).parent().find('input[type=number]')
            // var rows_selected = table.column(0).checkboxes.selected();
            //
            //
            // // Iterate over all selected checkboxes
            rec = {}
            rec.job_id = $('#job-id').val()
            rec.data = []
            $.each(inputs, function (index, rowId) {

                rec.data.push({'id':$(this).siblings('span').text(),'rating':inputs[index].value})
            });

            console.log("submiting")

            //ajax url to go to as per recommendation type
            recommendation_type = $('#job-type').val()
            if(recommendation_type == "TF-IDF")
                url = '/tfidf-rate/';
            else if(recommendation_type == "Word2vec")
                url = '/w2v-rate/';
            else
                url = '/wmd-rate/';
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    jsonData: JSON.stringify(rec),
                    csrfmiddlewaretoken: $('input[name=csrfmiddlewaretoken]').val()
                },
                headers: {"X-CSRFToken": getCookie("csrftoken")},
                dataType: "json",
                success: function (data) {

                    if(data.success){
                        $('#submit-rating').val('Done')
                        $('#submit-rating').prop('disabled', true)

                    }else{
                        $('#submit-rating').text('Error')
                    }
                },
                error:function (e) {
                    console.log(e.get_message())

                }
            });
        }
    );
}