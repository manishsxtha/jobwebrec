from django.contrib import admin

# Register your models here.
from jobs import models

admin.site.register(models.Job)
admin.site.register(models.Recommendtfidf)
admin.site.register(models.Recommendw2v)
admin.site.register(models.RecommendWMD)
admin.site.register(models.RateRecommendtfidf)
admin.site.register(models.RateRecommendw2v)
admin.site.register(models.RateRecommendWMD)