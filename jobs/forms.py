from django import forms


class SearchForm(forms.Form):
    job_title = forms.CharField(
        max_length=100, widget=forms.TextInput(
            attrs={
                'class': 'form-control standard',
                'placeholder':'eg:Java developer'
            }
        ),
    )


class RecommendationForm(forms.Form):
    rating = forms.CharField(
        max_length=100, widget=forms.TextInput(
            attrs={
                'class': 'form-control standard',
                'placeholder':'eg:Java developer'
            }
        ),
    )
