import os

import pandas as pd

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jobwebrec.settings')

import django

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from jobs.models import *

# firstly job dataset of filtered_software_Developer_Data.csv must be imported from web 'import from csv' index page

# import from database of recommendations of td-df of all jobs
job_rec_tfidf_csv = pd.read_csv('job_recommendations_tfidf.csv')
for row, job in job_rec_tfidf_csv.iterrows():
    job_id = job['job__id']
    job_rec_id = job['rec_job_id']
    rec_job_score = job['rec_job_score']

    job = Job.objects.get(id=job_id)
    Recommendtfidf.objects.get_or_create(
        job=job, rec_job_score=rec_job_score, job_rec_id=job_rec_id
    )

job_rec_word2vec_csv = pd.read_csv('job_recommendations_w2v.csv')

for row, job in job_rec_word2vec_csv.iterrows():
    job_id = job['job__id']
    job_rec_id = job['rec_job_id']
    rec_job_score = job['rec_job_score']

    job = Job.objects.get(id=job_id)
    Recommendw2v.objects.get_or_create(
        job=job, rec_job_score=rec_job_score, job_rec_id=job_rec_id
    )

job_rec_word2vec_wmd_csv = pd.read_csv('job_recommendations_word2vec_wmd.csv')
for row, job in job_rec_word2vec_wmd_csv.iterrows():
    job_id = job['job__id']
    job_rec_id = job['rec_job_id']
    rec_job_score = job['rec_job_score']

    job = Job.objects.get(id=job_id)
    RecommendWMD.objects.get_or_create(
        job=job, rec_job_score=rec_job_score, job_rec_id=job_rec_id
    )
