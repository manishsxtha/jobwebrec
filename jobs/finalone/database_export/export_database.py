import os

import numpy as np
import pandas as pd

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jobwebrec.settings')

import django

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from jobs.models import *

from django.shortcuts import render
from django.template import loader
from django_pandas.io import read_frame

# export from database of recommendations of td-df of all jobs
job_data = Recommendtfidf.objects.all()

df_test = pd.DataFrame.from_records(
    Recommendtfidf.objects.values(
        "id",
        "job__id",  # <-- get the name through the foreign key
        "rec_job_id",
        "rec_job_score",
    )
)


df_test.to_csv("job_recommendations_tfidf.csv")

# export from database of recommendatxions of word2vec cosine of all jobs

df_test2 = pd.DataFrame.from_records(
    Recommendw2v.objects.values(
        "id",
        "job__id",  # <-- get the name through the foreign key
        "rec_job_id",
        "rec_job_score",
    )
)


df_test2.to_csv("job_recommendations_w2v.csv")

# export from database of recommendations of word2vec wmd of all jobs


df_test3 = pd.DataFrame.from_records(
    RecommendWMD.objects.values(
        "id",
        "job__id",  # <-- get the name through the foreign key
        "rec_job_id",
        "rec_job_score",
    )
)


df_test3.to_csv("job_recommendations_w2v_wmd.csv")
