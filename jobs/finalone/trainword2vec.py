# %% md

## Importing libraries

# %%

import nltk
import numpy as np
import pandas as pd
from gensim.models import Word2Vec

# %% md
## Load the data
# %% md
# %%

df_jobs = pd.read_csv("word2vec_dataset_final.csv")
df_jobs.head()

# %%

df_jobs.info()

# %% md

## Exploratory Data Analysis

# %% md

#### First check the NA's

# %%

df_jobs.isnull().sum()

# %% md

#### Selecting the columns for the jobs corpus


# %%


# %%

nltk.download('punkt')
nltk.download('stopwords')

# %%


# %% md

### Word2vec


# %%

# !/usr/bin/env python

import nltk

from nltk.corpus import stopwords


class KaggleWord2VecUtility(object):
    """KaggleWord2VecUtility is a utility class for processing raw HTML text into segments for further learning"""

    @staticmethod
    def sku_to_wordlist(sku, remove_stopwords=False):
        # Function to convert a document to a sequence of words,
        # optionally removing stop words.  Returns a list of words.
        #
        # 1. Remove HTML
        # sku_text = BeautifulSoup(sku).get_text()
        #
        # 2. Remove non-letters
        # sku_text = re.sub("[^a-zA-Z]", " ", sku_text)
        #
        # 3. Convert words to lower case and split them
        words = sku.lower().split()
        #
        # 4. Optionally remove stop words (false by default)
        if remove_stopwords:
            stops = set(stopwords.words("english"))
            words = [w for w in words if not w in stops]
        #
        # 5. Return a list of words
        return (words)

    # Define a function to split a sku into parsed sentences
    @staticmethod
    def sku_to_sentences(sku, tokenizer, remove_stopwords=False):
        # Function to split a sku into parsed sentences. Returns a
        # list of sentences, where each sentence is a list of words
        #
        # 1. Use the NLTK tokenizer to split the paragraph into sentences
        raw_sentences = []
        try:
            sku = sku.encode('utf-8')
            raw_sentences = tokenizer.tokenize(sku.decode('utf8').strip())
        except:
            print("exception")
            pass
        # raw_sentences = tokenizer.tokenize(codecs.utf_8_encode(sku.decode('utf8').strip()))
        #
        # 2. Loop over each sentence
        sentences = []
        for raw_sentence in raw_sentences:
            # If a sentence is empty, skip it
            if len(raw_sentence) > 0:
                # Otherwise, call sku_to_wordlist to get a list of words
                sentences.append(KaggleWord2VecUtility.sku_to_wordlist(raw_sentence, \
                                                                       remove_stopwords))
        #
        # Return the list of sentences (each sentence is a list of words,
        # so this returns a list of lists
        return sentences


# %%


# Training the model
# Gensim Word2Vec Implementation:
# We use Gensim implementation of word2vec: https://radimrehurek.com/gensim/models/word2vec.html
import multiprocessing

# %%

# Why I seperate the training of the model in 3 steps:
# I prefer to separate the training in 3 distinctive steps for clarity and monitoring.
#
# Word2Vec():
# In this first step, I set up the parameters of the model one-by-one.
# I do not supply the parameter sentences, and therefore leave the model uninitialized, purposefully.
#
# .build_vocab():
# Here it builds the vocabulary from a sequence of sentences and thus initialized the model.
# With the loggings, I can follow the progress and even more important, the effect of min_count and sample on the word corpus. I noticed that these two parameters, and in particular sample, have a great influence over the performance of a model. Displaying both allows for a more accurate and an easier management of their influence.
#
# .train():
# Finally, trains the model.
# The loggings here are mainly useful for monitoring, making sure that no threads are executed instantaneously.
cores = multiprocessing.cpu_count()  # Count the number of cores in a computer

# Verify the number of skucollection that were read (100,000 in total)
print("Read %d labeled train skucollection " % df_jobs["text"].size)

# Load the punkt tokenizer
tokenizer = nltk.data.load('tokenizers/punkt/english.pickle')

# ****** Split the labeled and unlabeled training sets into clean sentences
#
sentences = []  # Initialize an empty list of sentences

print("Parsing sentences from training set")
for sku in df_jobs["text"]:
    sentences += KaggleWord2VecUtility.sku_to_sentences(sku, tokenizer)


# Set values for various parameters
num_features = 300  # Word vector dimensionality
min_word_count = 2  # Minimum word count
num_workers = cores - 1  # Number of threads to run in parallel
context = 5  # Context window size
skip_gram = 1  # skip-gram is for sg
cbow = 1  # cbow is 0  for sg

# Initialize and train the model (this will take some time)
print("Training Word2Vec model...")
w2v_model = Word2Vec(sentences, workers=num_workers, \
                     size=num_features, min_count=min_word_count, \
                     window=context)

# %%

# Training of the model:
# Parameters of the training:
#

# t = time()

w2v_model.train(sentences, total_examples=w2v_model.corpus_count, epochs=30, report_delay=1)

# print('Time to train the model: {} mins'.format(round((time() - t) / 60, 2)))

# %%

# As we do not plan to train
# the model any further, we are calling init_sims(), which will
# make the model much more memory-efficient:
w2v_model.init_sims(replace=True)

#
# # It can be helpful to create a meaningful model name and
# # save the model for later use. You can load it later using Word2Vec.load()
uni_model_name = "word2vecmodelv2.txt"

w2v_model.wv.save_word2vec_format(uni_model_name)
