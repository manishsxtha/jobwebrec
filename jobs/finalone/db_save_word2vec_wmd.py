# %%
import os

import gensim
import pandas as pd



os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jobwebrec.settings')

import django

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from jobs.models import Job, RecommendWMD

df_all = pd.read_csv("test2.csv")
df_all = df_all.fillna("")

# python -m spacy init-model en spacy.word2vec.model --vectors-loc word2vecmodel.txt.gz

from pyemd import emd
from gensim.similarities import WmdSimilarity
model = gensim.models.KeyedVectors.load_word2vec_format('word2vecmodel.txt.gz')

# Import and download stopwords from NLTK.
from nltk.corpus import stopwords
from nltk import download

download('stopwords')  # Download stopwords list.
def get_recommendation(job_id):
    recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
    count = 0
    start_job = Job.objects.get(id=job_id)
    start_job_text = start_job.job_title + " "+start_job.job_description
    all_jobs = Job.objects.all()
    for i,check_rec_job in enumerate(all_jobs):

        r = check_rec_job.id
        rec_job = Job.objects.get(id=r)
        rec_text = rec_job.job_description

        distance = model.wmdistance(start_job_text, rec_text)
        # print('distance = %.4f' % distance)
        recommendation.at[count, 'ApplicantID'] = u
        recommendation.at[count, 'JobID'] = check_rec_job.id
        recommendation.at[count, 'title'] = check_rec_job.job_title
        recommendation.at[count, 'score'] = distance
        count += 1

    recomm = recommendation.sort_values(by=['score']).head(10)
    print(recomm['JobID'])
    for i in range(len(recomm)):

        RecommendWMD.objects.get_or_create(
            job=Job.objects.get(id =u),
            rec_job_id=recomm['JobID'].iloc[i],
            rec_job_title=recomm['title'].iloc[i],
            rec_job_score=recomm['score'].iloc[i]
        )
    return recomm



all_jobs = Job.objects.all()[:100]
count = 0
for job in all_jobs:
    u = job.id
    print("job :"+str(count))
    get_recommendation(u)
    count = count+1









