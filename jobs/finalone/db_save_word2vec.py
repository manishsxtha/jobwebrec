# %%
import os

import numpy
import pandas as pd
import spacy

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jobwebrec.settings')

import django

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from jobs.models import *

df_all = pd.read_csv("test2.csv")
df_all = df_all.fillna("")

# python -m spacy init-model en spacy.word2vec.model --vectors-loc word2vecmodel.txt.gz
nlp = spacy.load('spacy.word2vecv2.model/')
#
# nlp = spacy.load('/usr/local/lib/python3.6/dist-packages/en_core_web_lg/en_core_web_lg-2.1.0')
#
# %% md

#### Transform the copurs text to the *spacy's documents*

# %%


list_docs = []
for i in range(len(df_all)):
    text = df_all['text'][i]

    doc = nlp("u'" + text + "'")
    list_docs.append((doc, i))



def calculateSimWithSpaCy(nlp, df, user_text, n=6):
    # Calculate similarity using spaCy
    print('Calculate similarity using spaCy')
    list_sim = []
    doc1 = nlp("u'" + user_text + "'")
    for i in df.index:
        try:
            doc2 = list_docs[i][0]
            score = doc1.similarity(doc2)
            list_sim.append((doc1, doc2, list_docs[i][1], score))
        except:
            continue

    print('Done')
    return list_sim


def get_recommendation(top, df_all, scores):
    recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
    count = 0
    for i in top:
        if (scores[count] < 0.99999):
            Recommendw2v.objects.get_or_create(
                job=getjob,
                rec_job_id=df_all['Job.ID'][i],
                rec_job_title=df_all['Title'][i],
                rec_job_score=scores[count]
            )
            recommendation.at[count, 'ApplicantID'] = u
            recommendation.at[count, 'JobID'] = df_all['Job.ID'][i]
            recommendation.at[count, 'title'] = df_all['Title'][i]
            recommendation.at[count, 'score'] = scores[count]
        count += 1
    return recommendation


all_jobs = Job.objects.all()[10]
for job in all_jobs:
    u = job.id
    index = numpy.where(df_all['Job.ID'] == u)[0][0]
    user_q = df_all.iloc[[index]]
    user_q
    getjob = Job.objects.get(id=u)

    df3 = calculateSimWithSpaCy(nlp, df_all, user_q.text.iloc[0], n=15)
    print('spacy sim is')

    df_recom_spacy = pd.DataFrame(df3).sort_values([3], ascending=False).head(11)
    #
    # %%

    df_recom_spacy.reset_index(inplace=True)
    #
    #
    # %%

    index_spacy = df_recom_spacy[2]
    list_scores = df_recom_spacy[3]
    #
    # %% md

    ## The Top recommendations using Spacy

    # %%

    print(get_recommendation(index_spacy, df_all, list_scores))
    #


