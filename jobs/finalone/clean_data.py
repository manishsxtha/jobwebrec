import nltk
import numpy as np
import pandas as pd

df_jobs = pd.read_csv("software_developer.csv")
df_jobs.head()

# %%

df_jobs.info()

df_jobs.isnull().sum()



df_jobs = df_jobs[['job_title','category','company_name','city','job_description']]
# %%


# %% md

#### Let´s check the NA's by plotting them

# %%




df_nan_city = df_jobs[pd.isnull(df_jobs['City'])]
print(df_nan_city.shape)
df_nan_city.head()

# %%

df_nan_city.groupby(['Company'])['City'].count()

# %% md

#### We see that there are only 9 companies cities that are having NaN values so it must be manually adding their head quarters (by simply searching at google)


# %%

# replacing nan with thier headquarters location
df_jobs['Company'] = df_jobs['Company'].replace(['Genesis Health Systems'], 'Genesis Health System')
df_jobs.loc[df_jobs.Company == 'CHI Payment Systems', 'City'] = 'Illinois'
df_jobs.loc[df_jobs.Company == 'Academic Year In America', 'City'] = 'Stamford'
df_jobs.loc[df_jobs.Company == 'CBS Healthcare Services and Staffing ', 'City'] = 'Urbandale'
df_jobs.loc[df_jobs.Company == 'Driveline Retail', 'City'] = 'Coppell'
df_jobs.loc[df_jobs.Company == 'Educational Testing Services', 'City'] = 'New Jersey'
df_jobs.loc[df_jobs.Company == 'Genesis Health System', 'City'] = 'Davennport'
df_jobs.loc[df_jobs.Company == 'Home Instead Senior Care', 'City'] = 'Nebraska'
df_jobs.loc[df_jobs.Company == 'St. Francis Hospital', 'City'] = 'New York'
df_jobs.loc[df_jobs.Company == 'Volvo Group', 'City'] = 'Washington'
df_jobs.loc[df_jobs.Company == 'CBS Healthcare Services and Staffing', 'City'] = 'Urbandale'

# %%

df_jobs.isnull().sum()

# %%

# The employement type NA are from Uber so I assume as part-time and full time
df_nan_emp = df_jobs[pd.isnull(df_jobs['Empl_type'])]
df_nan_emp.head()

# %%

df_jobs['Empl_type'].unique()

# %%

# replacing na values with part time/full time
df_jobs['Empl_type'] = df_jobs['Empl_type'].fillna('Full-Time/Part-Time')
df_jobs.groupby(['Empl_type'])['Company'].count()
df_jobs.head()

# %% md

##  Creating the jobs corpus

# %% md

#### combining the columns of position, company, city, emp_type and position

# %%

df_jobs["text"] = df_jobs["Position"].map(str) + " " + df_jobs["Company"] + " " + df_jobs["City"] + " " + df_jobs[
    'Empl_type'] + " " + df_jobs['Job_Description'] + " " + df_jobs['Title']
df_jobs.head(2)

# %%

df_all = df_jobs[['Job.ID', 'text', 'Title']]

df_all = df_all.fillna(" ")

df_all.head()

# %%

df_all.shape

# %%

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')

# %%


import re
import string
from nltk.stem import WordNetLemmatizer
from nltk import word_tokenize
from nltk.corpus import stopwords

stop = stopwords.words('english')
stop_words_ = set(stopwords.words('english'))
wn = WordNetLemmatizer()


def black_txt(token):
    return token not in stop_words_ and token not in list(string.punctuation) and len(token) > 2


def clean_txt(text):
    clean_text = []
    clean_text2 = []
    text = re.sub("'", "", text)
    text = re.sub("(\\d|\\W)+", " ", text)
    text = text.replace("nbsp", "")
    clean_text = [wn.lemmatize(word, pos="v") for word in word_tokenize(text.lower()) if black_txt(word)]
    clean_text2 = [word for word in clean_text if black_txt(word)]
    return " ".join(clean_text2)


# %% md

#### Cleaning the jobs corpus

# %%

df_all['text'] = df_all['text'].apply(clean_txt)

df_all.to_csv('test.csv')