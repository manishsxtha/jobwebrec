import os

import numpy as np
import pandas as pd

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'jobwebrec.settings')

import django

os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from jobs.models import *
from sklearn.metrics.pairwise import cosine_similarity

# %% md

#### Finally we ended we a clean text for the jobs dataset:

# %%

df_all = pd.read_csv("filtered_software_developer_data.csv")
df_all = df_all.fillna("")

# %% md

####TF-IDF ( Term Frequency - Inverse Document Frequency )


# %%


# initializing tfidf vectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

tfidf_vectorizer = TfidfVectorizer()

tfidf_jobid = tfidf_vectorizer.fit_transform((df_all['text']))  # fitting and transforming the vector


def get_recommendation(top, df_all, scores):
    recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
    count = 0
    for i in top:

        if (scores[count] < 0.99999):

            Recommendtfidf.objects.get_or_create(
                job=getjob,
                rec_job_id=df_all['Job.ID'][i],
                rec_job_title=df_all['Title'][i],
                rec_job_score=scores[count]
            )
            recommendation.at[count, 'ApplicantID'] = u
            recommendation.at[count, 'JobID'] = df_all['Job.ID'][i]
            recommendation.at[count, 'title'] = df_all['Title'][i]
            recommendation.at[count, 'score'] = scores[count]
        count += 1
    return recommendation


print("Starting job recommendation for 1000 jobs")
all_jobs = Job.objects.all()[:100]
for row, job in enumerate(all_jobs):
    u = job.id
    print("Job id: " + str(row))
    index = np.where(df_all['Job.ID'] == u)[0][0]
    user_q = df_all.iloc[[index]]
    user_q
    getjob = Job.objects.get(id=u)
    user_tfidf = tfidf_vectorizer.transform(user_q['text'])
    cos_similarity_tfidf = map(lambda x: cosine_similarity(user_tfidf, x), tfidf_jobid)
    output2 = list(cos_similarity_tfidf)
    top = sorted(range(len(output2)), key=lambda i: output2[i], reverse=True)[:11]

    list_scores = [output2[i][0][0] for i in top]
    recommendations = get_recommendation(top, df_all, list_scores)
    print('Job recommendations for ' + job.job_title)
    print(recommendations)
