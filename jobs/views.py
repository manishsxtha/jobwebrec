import json
import os

import gensim
import numpy
import numpy as np
import pandas as pd
import spacy
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.http import HttpResponse
# Create your views here.
from django.shortcuts import render
from django.template import loader
from django_pandas.io import read_frame
# Import and download stopwords from NLTK.
from nltk import download
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

from jobs.forms import SearchForm
from jobs.models import Job, Recommendtfidf, RateRecommendtfidf, Recommendw2v, RecommendWMD, RateRecommendw2v, \
    RateRecommendWMD

download('stopwords')  # Download stopwords list.


def index(request):
    template = loader.get_template('jobs/index.html')
    context = {}
    return HttpResponse(template.render(context, request))


def job_detail(request, job_id):
    job_single = Job.objects.get(id=job_id)
    context = {
        'job': job_single
    }
    return render(request, "jobs/job_single.html", context)


def job_list(request):
    form = SearchForm()
    if request.method == 'POST':
        form = SearchForm(request.POST)
        if form.is_valid():

            job_title = form.cleaned_data["job_title"]
            print(job_title)
            job_data = Job.objects.filter(Q(job_title__contains=job_title) | Q(category__contains=job_title) |
                                          Q(job_description__contains=job_title))
            jobs = paginate(request, job_data)

            return render(request, 'jobs/jobs_search.html', {
                'form': form,
                'jobs': jobs
            })
        else:
            return render(request, 'jobs/jobs_search.html', {
                'form': form,

            })
    job_data = Job.objects.all()

    jobs = paginate(request, job_data)
    return render(request, 'jobs/jobs_search.html', {
        'form': form,
        'jobs': jobs
    })


def paginate(request, data):
    page = request.GET.get('page', 1)
    paginator = Paginator(data, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    return data


def import_jobs(request):
    if request.method == 'POST' and request.FILES['jobfile']:
        success = ""

        myfile = request.FILES['jobfile']
        try:
            jobcsv = pd.read_csv(myfile)
            jobcsv['Title'] = jobcsv['Title'].fillna("")
            jobcsv['category'] = jobcsv['category'].fillna("")
            jobcsv['job_description'] = jobcsv['job_description'].fillna("")
            jobcsv['city'] = jobcsv['city'].fillna("")
            jobcsv['company_name'] = jobcsv['company_name'].fillna("")
            print(jobcsv)
            for row, job in jobcsv.iterrows():
                print(job["Title"])
                id = job['Job.ID']
                title = job['Title']
                category = job['category']
                job_description = job['job_description']
                city = job['city']
                company = job['company_name']
                # print(id)

                job_model = Job.objects.create(id=id, job_title=title, job_description=job_description, city=city,
                                               category=category, company=company)

            success = "Done"
        except Exception as e:
            print(e)
            success = "Error"

        return render(request, 'jobs/jobs_upload.html', {
            'success': success,
        })
    return render(request, 'jobs/jobs_upload.html')


results = {}


def recommendations_tfidf(request, job_id):
    start_job = Job.objects.get(id=job_id)
    job_data = Recommendtfidf.objects.filter(job__id=job_id)

    print(job_data)
    # if data is not processed
    if not job_data:

        qs = Job.objects.all()
        df_all = read_frame(qs)

        df_all['text'] = df_all['job_title'] + df_all['category'] + df_all['job_description'] + df_all['city'] + df_all[
            'company']

        tfidf_vectorizer = TfidfVectorizer()

        tfidf_jobid = tfidf_vectorizer.fit_transform((df_all['text']))  # fitting and transforming the vector

        def get_recommendation(top, df_all, scores):
            recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
            count = 0
            for i in top:

                if (scores[count] < 0.99999):
                    Recommendtfidf.objects.get_or_create(
                        job=getjob,
                        rec_job_id=df_all['id'][i],
                        rec_job_title=df_all['job_title'][i],
                        rec_job_score=scores[count]
                    )
                    recommendation.at[count, 'ApplicantID'] = u
                    recommendation.at[count, 'JobID'] = df_all['id'][i]
                    recommendation.at[count, 'title'] = df_all['job_title'][i]
                    recommendation.at[count, 'score'] = scores[count]
                count += 1
            return recommendation

        u = job_id

        index = np.where(df_all['id'] == u)[0][0]
        user_q = df_all.iloc[[index]]
        getjob = Job.objects.get(id=u)
        user_tfidf = tfidf_vectorizer.transform(user_q['text'])
        cos_similarity_tfidf = map(lambda x: cosine_similarity(user_tfidf, x), tfidf_jobid)

        output2 = list(cos_similarity_tfidf)

        top = sorted(range(len(output2)), key=lambda i: output2[i], reverse=True)[:11]
        list_scores = [output2[i][0][0] for i in top]
        recommendations = get_recommendation(top, df_all, list_scores)
        print(recommendations)
        job_data = Recommendtfidf.objects.filter(job__id=job_id)
    recommended_jobs = []
    for job in job_data:
        j = Job.objects.get(id=job.rec_job_id)
        recommended_jobs.append({
            'id': job.rec_job_id,
            'title': job.rec_job_title,
            'desc': j.job_description,
            'score': job.rec_job_score
        }
        )

    return render(request, 'jobs/job_recommendations.html', {
        'jobs': recommended_jobs,
        'job': start_job,
        'type': "TF-IDF"
    })


def recommendations_word2vec(request, job_id):
    start_job = Job.objects.get(id=job_id)
    job_data = Recommendw2v.objects.filter(job__id=job_id)
    # if data is not processed
    if not job_data:
        qs = Job.objects.all()
        df_all = read_frame(qs)
        dirspot = os.getcwd() + "/jobs/finalone/spacy.word2vecv2.model"

        nlp = spacy.load(dirspot)
        df_all['text'] = df_all['job_title'] + df_all['category'] + df_all['job_description'] + df_all['city'] + df_all[
            'company']

        list_docs = []
        for i in range(len(df_all)):
            text = df_all['text'][i]

            doc = nlp("u'" + text + "'")
            list_docs.append((doc, i))

        def calculateSimWithSpaCy(nlp, df, user_text, n=6):
            # Calculate similarity using spaCy
            print('Calculate similarity using spaCy')
            list_sim = []
            doc1 = nlp("u'" + user_text + "'")
            for i in df.index:
                try:
                    doc2 = list_docs[i][0]
                    score = doc1.similarity(doc2)
                    list_sim.append((doc1, doc2, list_docs[i][1], score))
                except:
                    continue

            print('Done')
            return list_sim

        def get_recommendation(top, df_all, scores):
            recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
            count = 0
            for i in top:
                if (scores[count] < 0.99999):
                    Recommendw2v.objects.get_or_create(
                        job=getjob,
                        rec_job_id=df_all['id'][i],
                        rec_job_title=df_all['job_title'][i],
                        rec_job_score=scores[count]
                    )
                    recommendation.at[count, 'ApplicantID'] = u
                    recommendation.at[count, 'JobID'] = df_all['id'][i]
                    recommendation.at[count, 'title'] = df_all['job_title'][i]
                    recommendation.at[count, 'score'] = scores[count]
                count += 1
            return recommendation

        u = job_id
        index = numpy.where(df_all['id'] == u)[0][0]
        selected_job = df_all.iloc[[index]]

        getjob = Job.objects.get(id=u)

        df3 = calculateSimWithSpaCy(nlp, df_all, selected_job.text.iloc[0], n=15)

        df_recom_spacy = pd.DataFrame(df3).sort_values([3], ascending=False).head(11)

        df_recom_spacy.reset_index(inplace=True)

        index_spacy = df_recom_spacy[2]
        list_scores = df_recom_spacy[3]

        print(get_recommendation(index_spacy, df_all, list_scores))
        # load it from the database
        job_data = Recommendtfidf.objects.filter(job__id=job_id)

    recommended_jobs = []
    for job in job_data:
        j = Job.objects.get(id=job.rec_job_id)
        recommended_jobs.append({
            'id': job.rec_job_id,
            'title': job.rec_job_title,
            'desc': j.job_description,
            'score':job.rec_job_score
        }
        )

    return render(request, 'jobs/job_recommendations.html', {
        'jobs': recommended_jobs,
        'job': start_job,
        'type': "Word2vec"
    })


def recommendations_word2vec_wmd(request, job_id):
    start_job = Job.objects.get(id=job_id)
    job_data = RecommendWMD.objects.filter(job__id=job_id)

    if not job_data:
        qs = Job.objects.all()
        df_all = read_frame(qs)

        dirspot = os.getcwd() + "/jobs/finalone/word2vecmodel.txt.gz"
        model = gensim.models.KeyedVectors.load_word2vec_format(dirspot)

        def get_recommendation(job_id):
            recommendation = pd.DataFrame(columns=['ApplicantID', 'JobID', 'title', 'score'])
            count = 0
            start_job = Job.objects.get(id=job_id)
            start_job_text = start_job.job_title + " " + start_job.job_description
            all_jobs = Job.objects.all()
            for i, check_rec_job in enumerate(all_jobs):
                r = check_rec_job.id
                rec_job = Job.objects.get(id=r)
                rec_text = rec_job.job_description

                distance = model.wmdistance(start_job_text, rec_text)
                # print('distance = %.4f' % distance)
                recommendation.at[count, 'ApplicantID'] = job_id
                recommendation.at[count, 'JobID'] = check_rec_job.id
                recommendation.at[count, 'title'] = check_rec_job.job_title
                recommendation.at[count, 'score'] = distance
                count += 1

            recomm = recommendation.sort_values(by=['score']).head(10)
            print(recomm['JobID'])
            for i in range(len(recomm)):
                RecommendWMD.objects.get_or_create(
                    job=Job.objects.get(id=job_id),
                    rec_job_id=recomm['JobID'].iloc[i],
                    rec_job_title=recomm['title'].iloc[i],
                    rec_job_score=recomm['score'].iloc[i]
                )
            return recomm

        get_recommendation(job_id)
        job_data = Recommendtfidf.objects.filter(job__id=job_id)

    recommended_jobs = []
    for job in job_data:
        j = Job.objects.get(id=job.rec_job_id)
        recommended_jobs.append({
            'id': job.rec_job_id,
            'title': job.rec_job_title,
            'desc': j.job_description,
            'score': job.rec_job_score
        }
        )

    return render(request, 'jobs/job_recommendations.html', {
        'jobs': recommended_jobs,
        'job': start_job,
        'type': "WMD"
    })


def tfidf_rating(request):
    if request.method == 'POST':

        data = json.loads(request.POST['jsonData'])
        job_id = data['job_id']
        print(job_id)
        print(data)
        for rating in data['data']:
            recommended_job_id = rating['id']
            recommended_job_rating = rating['rating']

            job = Job.objects.get(id=job_id)
            user = User.objects.first()
            recommendtfidf = Recommendtfidf.objects.get(job__id=job_id, rec_job_id=recommended_job_id)

            RateRecommendtfidf.objects.create(user=user,job=job, recommendtfidf=recommendtfidf, rating=recommended_job_rating)
        message = {
            'success': True,
        }
        return HttpResponse(json.dumps(message), content_type='application/json')


def word2vec_rating(request):
    if request.method == 'POST':

        data = json.loads(request.POST['jsonData'])
        job_id = data['job_id']
        print(data)
        for rating in data['data']:
            recommended_job_id = rating['id']
            recommended_job_rating = rating['rating']

            user = User.objects.first()
            job = Job.objects.get(id=job_id)
            recommedw2v = Recommendw2v.objects.get(job__id=job_id, rec_job_id=recommended_job_id)
            RateRecommendw2v.objects.create(user=user,job=job, recommendw2v=recommedw2v, rating=recommended_job_rating)
        message = {
            'success': True,
        }
        return HttpResponse(json.dumps(message), content_type='application/json')


def word2vec_wmd_rating(request):
    if request.method == 'POST':

        data = json.loads(request.POST['jsonData'])
        job_id = data['job_id']
        print(data)
        for rating in data['data']:
            recommended_job_id = rating['id']
            recommended_job_rating = rating['rating']

            job = Job.objects.get(id=job_id)
            user = User.objects.first()
            recommendWMD = RecommendWMD.objects.get(job__id=job_id, rec_job_id=recommended_job_id)
            RateRecommendWMD.objects.create(user= user,job=job, recommendWMD=recommendWMD, rating=recommended_job_rating)
        message = {
            'success': True,
        }
        return HttpResponse(json.dumps(message), content_type='application/json')
#
# def recommendations_word2vec(request, job_id):
#     form = SearchForm()
#
#     qs = Job.objects.all()
#     df_jobs = read_frame(qs)
#     module_dir = os.path.dirname(__file__)  # get current directory
#     file_path = os.path.join(module_dir, '300features_40minwords_10_cbow.model')  # full path to text.
#     data_file = open(file_path, 'rb')  # I used'rb' here since I got an 'gbk' decode error
#     data = data_file.read()
#
#     print("loading w2vec model")
#     w2v_model = gensim.models.Word2Vec.load(data)
#     print("done")
#     # add new column of title and description
#     sum_title_desc = df_jobs['job_title'] + df_jobs['job_description']
#     df_jobs['title_desc'] = sum_title_desc
#     final_recommendations = []
#
#     def most_similar(item_id):
#         try:
#             print("Similar of " + df_jobs[df_jobs['id'] == int(item_id)].iloc[0]['job_title'])
#         except:
#             print("Similar of " + item_id)
#         return [(x, df_jobs[df_jobs['id'] == int(x[0])].iloc[0]['job_title']) for x in
#                 w2v_model.wv.most_similar(item_id)]
#
#     most_similar(job_id)
#     execution = timeit.timeit()
#     print("Execution time : " + str(execution))
#
#     # job_data = Job.objects.all()
#     #
#     # jobs = paginate(request, job_data)
#
#     return render(request, 'jobs/job_recommendations.html', {
#         'form': form,
#         'jobs': final_recommendations,
#         'type': "Word2vec"
#     })
