from django.contrib.auth import authenticate
from django.http import HttpResponse


from django.shortcuts import render

# Create your views here.
from django.template import loader

from login.forms import LoginForm


def login(request):
    form = LoginForm()
    context = {}
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():

            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)

            if user is not None:
                request.session['member_id'] = user.id
                template = loader.get_template('jobs/index.html')
                context = {}
                return HttpResponse(template.render(context, request))
            else:
                return render(request, "login/login.html", {
                    'form': form,
                    'authenticated':False,
                    'attempt':True
                })
        else:
            return render(request, "login/login.html", {
                'form': form,
                'authenticated': False,
                'attempt': True
            })
    return render(request, "login/login.html", {
        'form':form,
        'authenticated':False,
        'attempt':False})


def logout(request):
    try:
        request.session['member_id'] = 0
    except KeyError:
        pass
    template = loader.get_template('jobs/index.html')
    context = {}
    return HttpResponse(template.render(context, request))